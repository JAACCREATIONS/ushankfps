﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsEntity3 : MonoBehaviour
{

	public Rigidbody       rb;
	public CapsuleCollider collider;

	public Transform root;

	public LayerMask groundLayerMask;

	[Header("Input")]
	public KeyCode forwardsKey = KeyCode.W;
	public KeyCode backwardKey = KeyCode.S;
	public KeyCode leftKey = KeyCode.A;
	public KeyCode rightKey = KeyCode.D;
	
//	public KeyCode jumpKey = KeyCode.Space;
	
	public bool forwardInput;
	public bool backwardInput;
	public bool leftInput;
	public bool rightInput;

//	public bool jumpInput;
	
	public Vector3 inputAxis;
	public Vector3 smoothInputAxis;
	public Vector3 groundRayAxis;
	
	[Header("Grounding")]
	[SerializeField]
	public float groundRayAxisRangeOffset = 0.0f;
	
	[SerializeField]
	public float groundRayLengthOffset = 0.05f;
	
	[SerializeField]
	private bool _onTheGround = false;
	public bool onTheGround
	{
		get => _onTheGround;
		set
		{
			if (_onTheGround == value) return;

			_onTheGround = value;

			if (_onTheGround)
			{
				HitTheGround();
			}
			else
			{
				LeftTheGround();
			}
		}
	}

	[Header("Horizontal Movement")]
	[Range(min: 0.1f, max: 2.0f)]
	public float horizontalAcceleration = 1.0f;
	
	[Range(min: 0.01f, max: 0.1f)]
	public float horizontalDeceleration = 0.03f;

	[Range(min: 1.0f, max: 10.0f)]
	public float maxHorizontalSpeed = 6.0f;

	public Vector3 horizontalMomentum;

	[Header("Vertical Movement")]

	public float gravitySeed = -2.0f;

	[Range(min: 0.1f, max: 2.0f)]
	public float gravityAcceleration = 1.05f;
	
	public float maxVerticalSpeed = 30.0f;

	public float verticalMomentum;

//	[Header("Jumping")]
//	public float jumpThrust;
//	
//	public float jumpSeed = 2.0f;
//
//	public float jumpDecayRate = 0.95f;
	
	void HitTheGround()
	{
//		rb.useGravity = false;

//		rb.drag       = groundDrag;

		var v = rb.velocity;

		v.y = 0;

		rb.velocity = v;

//		jumpThrust = 0.0f;
		
		verticalMomentum = 0;
	}


	void LeftTheGround()
	{
		verticalMomentum = gravitySeed;
		
//		rb.useGravity = true;

//		rb.drag = midAirDrag;

		rb.WakeUp();
	}


	void OnValidate()
	{

		if (groundLayerMask.value == 0)
		{
			groundLayerMask = LayerMask.GetMask("ground");
		}

		root     = transform;
		rb       = GetComponent<Rigidbody>();
		collider = GetComponent<CapsuleCollider>();
	}


	void Update()
	{
		forwardInput  = Input.GetKey(forwardsKey);
		backwardInput = Input.GetKey(backwardKey);
		leftInput     = Input.GetKey(leftKey);
		rightInput    = Input.GetKey(rightKey);
//		jumpInput     = Input.GetKey(jumpKey);



		inputAxis = new Vector3(x: (leftInput ? -1.0f : 0.0f) + (rightInput ? 1.0f : 0.0f),
		                        y: 0.0f,
		                        z: (backwardInput ? -1.0f : 0.0f) + (forwardInput ? 1.0f : 0.0f)).normalized;

		smoothInputAxis = Vector3.Lerp(a: smoothInputAxis,
		                               b: inputAxis,
		                               t: Time.deltaTime * 16.0f);

		groundRayAxis = smoothInputAxis * (collider.radius + groundRayAxisRangeOffset);

		var p = root.position;

		onTheGround = Physics.Raycast(origin: p + groundRayAxis,
		                              direction: Vector3.down,
		                              hitInfo: out var hit,
		                              maxDistance: RayHeight,
		                              layerMask: groundLayerMask);

		Debug.DrawRay(start: p + groundRayAxis,
		              dir: Vector3.down * RayHeight,
		              color: onTheGround ? Color.green : Color.red);

		if (onTheGround)
		{
//			if (Input.GetKeyDown(jumpKey))
//			{
//				jumpThrust = jumpSeed;
//			}

			if (verticalMomentum <= 0) // Only do this while falling, otherwise we can accidentally snap to the ground at the start of jump
			{
				p.y = hit.point.y + RayHeight;
			}
		}
//		else
//		{
//			if (Input.GetKeyUp(jumpKey))
//			{
//				jumpThrust = 0.0f;
//			}
//		}

		root.position = p;

	}


	void FixedUpdate()
	{
//		horizontalMomentum = rb.velocity;

		if (inputAxis.sqrMagnitude > Mathf.Epsilon)
		{
			horizontalMomentum += inputAxis * horizontalAcceleration;
		}
		else
		{
			var vel = Vector3.zero;
			
			horizontalMomentum = Vector3.SmoothDamp(current: horizontalMomentum,
			                                        target: Vector3.zero,
			                                        currentVelocity: ref vel,
			                                        smoothTime: horizontalDeceleration);
		}

		horizontalMomentum = Vector3.ClampMagnitude(vector: horizontalMomentum,
		                                            maxLength: maxHorizontalSpeed);

//		if (jumpInput)
//		{
//			verticalMomentum += jumpThrust;
//		}
		
		if (!onTheGround)
		{
//			if (jumpThrust > 0)
//			{
//				jumpThrust += jumpDecayRate;
//			}
//			else
//			{
//				jumpThrust = 0.0f;
//			}

			verticalMomentum *= gravityAcceleration;

//			verticalMomentum -= gravityAcceleration;

			
//			if (verticalMomentum > 0)
//			{
//				verticalMomentum *= 1 - gravityAcceleration;
//			}
//			else
//			{
//				verticalMomentum *= gravityAcceleration;
//			}

		}

		verticalMomentum = Mathf.Clamp(value: verticalMomentum,
		                               min: -maxVerticalSpeed,
		                               max: maxVerticalSpeed);

		rb.velocity = new Vector3(x: horizontalMomentum.x,
		                          y: verticalMomentum,
		                          z: horizontalMomentum.z);
	}


	private void OnDrawGizmosSelected()
	{
		if (Application.isPlaying)
		{
			Gizmos.color = Color.white;

			Gizmos.DrawSphere(center: root.position + groundRayAxis,
			                  radius: 0.1f);

			Gizmos.color = Color.black;

			Gizmos.DrawSphere(center: root.position + inputAxis,
			                  radius: 0.1f);

		}
	}


	private float RayHeight =>
		collider.height * 0.5f + groundRayLengthOffset;

//	float Speed => onTheGround ? groundSpeed : airSpeed;

	Vector3 Forward =>
		root.forward;

	Vector3 Backward =>
		-root.forward;

	Vector3 Left =>
		-root.right;

	Vector3 Right =>
		root.right;

}