﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(requiredComponent: typeof(Rigidbody),
                  requiredComponent2: typeof(CapsuleCollider))]
public abstract class PhysicsEntityBase : MonoBehaviour
{

	public Rigidbody       rb;
	public CapsuleCollider collider;

	public Transform root;
	public Transform camTrans;


	[Header("Input")]
	public KeyCode forwardsKey = KeyCode.W;
	public KeyCode backwardKey = KeyCode.S;
	public KeyCode leftKey     = KeyCode.A;
	public KeyCode rightKey    = KeyCode.D;

	public KeyCode jumpKey = KeyCode.Space;

	public bool forwardInput;
	public bool backwardInput;
	public bool leftInput;
	public bool rightInput;

	[SerializeField]
	private bool _jumpInput = false;
	public bool jumpInput
	{
		get => _jumpInput;
		set
		{
			if (_jumpInput == value) return;

			_jumpInput = value;

			if (_jumpInput)
			{
				JumpKeyDown();
			}
			else
			{
				JumpKeyReleased();
			}
		}
	}


	public Vector3 inputAxis;
	public Vector3 smoothInputAxis;

//	public Vector3 groundRayAxis;




	[Header("Grounding")]
	[FormerlySerializedAs("debugGroundRayHeightOffset")] 
	public float groundRayGroundRadius = 0.0f;
	public float groundRayAirRadius    = 0.0f;
	public float groundRayGroundLength = 0.0f;
	public float groundRayAirLength    = -2.0f;
	public float groundSnapOffset      = 0.1f;
	
	
	
	public LayerMask groundLayerMask;

	public float DynamicGroundRayRadius =>
		onTheGround ? groundRayGroundRadius : groundRayAirRadius;
	
	// If we're already on the ground, its beneficial to have a bit of extra ground-ray length to really secure the grounding.
	// But if we're mid-air, having that extra length can create an ugly snap when you land. We want it short in mid-air basically.
	public float DynamicGroundRayLength =>
		onTheGround ? groundRayGroundLength : groundRayAirLength;
	
	[SerializeField]
	private bool _onTheGround = false;
	public bool onTheGround
	{
		get => _onTheGround;
		set
		{
			if (_onTheGround == value) return;

			_onTheGround = value;

			if (_onTheGround)
			{
				HitTheGround();
			}
			else
			{
				LeftTheGround();
			}
		}
	}


	public virtual void OnValidate()
	{

		if (groundLayerMask.value == 0)
		{
			groundLayerMask = LayerMask.GetMask("ground");
		}

		root              = transform;
		rb                = GetComponent<Rigidbody>();
		rb.freezeRotation = true;
		collider          = GetComponent<CapsuleCollider>();
		collider.radius   = 0.333f;
		collider.height   = 1.5f;
		camTrans          = Camera.main.transform;
	}


	public virtual void Update()
	{
		forwardInput  = Input.GetKey(forwardsKey);
		backwardInput = Input.GetKey(backwardKey);
		leftInput     = Input.GetKey(leftKey);
		rightInput    = Input.GetKey(rightKey);

		jumpInput = Input.GetKey(jumpKey);

		inputAxis = new Vector3(x: (leftInput ? -1.0f : 0.0f) + (rightInput ? 1.0f : 0.0f),
		                        y: 0.0f,
		                        z: (backwardInput ? -1.0f : 0.0f) + (forwardInput ? 1.0f : 0.0f)).normalized;

		smoothInputAxis = Vector3.Lerp(a: smoothInputAxis,
		                               b: inputAxis,
		                               t: Time.deltaTime * 16.0f);

	}




	public virtual void HitTheGround()
	{
		rb.useGravity = false;

		var v = rb.velocity;

		v.y = 0.0f;

		rb.velocity = v;
	}


	public virtual void LeftTheGround()
	{
		rb.useGravity = true;
	}


	protected virtual void JumpKeyDown() { }

	protected virtual void JumpKeyReleased() { }


	protected virtual void OnDrawGizmosSelected()
	{
		if (Application.isPlaying)
		{
			Gizmos.color = Color.white;

			Gizmos.DrawSphere(center: root.position + smoothInputAxis,
			                  radius: 0.1f);

			Gizmos.color = Color.black;

			Gizmos.DrawSphere(center: root.position + inputAxis,
			                  radius: 0.1f);

		}
	}


	protected float DefaultRayLength =>
		collider.height * 0.5f;

	protected float GroundSnapHeight =>
		collider.height * 0.5f + groundSnapOffset;
	
	protected Vector3 LocalForward =>
		root.forward;

	protected Vector3 LocalBackward =>
		-root.forward;

	protected Vector3 LocalLeft =>
		-root.right;

	protected Vector3 LocalRight =>
		root.right;
	
	protected Vector3 CamForward =>
		camTrans.forward;

	protected Vector3 CamBackward =>
		-camTrans.forward;

	protected Vector3 CamLeft =>
		-camTrans.right;

	protected Vector3 CamRight =>
		camTrans.right;
}