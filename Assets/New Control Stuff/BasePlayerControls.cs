﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BasePlayerControls : MonoBehaviour
{
    
    // We could put some common things like rigidbodies and camera references in here.
    // However, if we leave it very bare-bones, it allows us to have some really open control schemes later on.
    // Like maybe you can control vehicles or machinery at some point!
    
    public static BasePlayerControls CurrentBasePlayerControls = null;

    protected virtual void OnEnable()
    {
        CurrentBasePlayerControls = this;
    }

    protected virtual void OnDisable()
    {
        
    }
}