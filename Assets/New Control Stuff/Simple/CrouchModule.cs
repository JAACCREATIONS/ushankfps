﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CrouchModule
{

	public KeyCode crouchKey = KeyCode.LeftControl;
	
	public float normalHeight = 1.5f;
	public float crouchHeight = 0.25f;

	private float currentHeightVelocity;
	
	public float smoothTime = 0.1f;

	public void Update(CapsuleCollider capsule) =>
		capsule.height = Mathf.SmoothDamp(current: capsule.height,
		                                  target: Input.GetKey(crouchKey) ? crouchHeight : normalHeight,
		                                  currentVelocity: ref currentHeightVelocity,
		                                  smoothTime: smoothTime);

}
