﻿using System;

using UnityEngine;

[Serializable]
public class EntityController : MonoBehaviour
{

	[Header(header: "References")] [SerializeField]
	public PlayerEntity player;
	[SerializeField]
	public Rigidbody rb;
	[SerializeField]
	public CapsuleCollider capsule;
	[SerializeField]
	public Transform firstPersonCam;
	[SerializeField]
	public Transform firstPersonMeshRoot;

	[Header(header: "Update")]
	public StrafeInputModule strafeInput;

	public MouseInputModule mouseInput;

	public MouseLookModule mouseLook;

	public ThirdPersonLookModule thirdPersonLook;

	public SwayModule sway;

	public BobModule cameraBob;

	public BobModule meshBob;

	public CrouchModule crouch;
	
	public JumpModule jump;
	
	[Header(header: "Fixed Update")]
	public StrafeVelocityModule strafeVelocity;

	public VelocityClampModule velocityClamp;


	public void OnValidate()
	{
		player  = GetComponent<PlayerEntity>();
		rb      = GetComponentInChildren<Rigidbody>();
		capsule = GetComponentInChildren<CapsuleCollider>();
	}


	public void Update()
	{
		strafeInput.Update();
		
		mouseInput.Update();

		mouseLook.Update(camTrans: firstPersonCam,
		                 x: mouseInput.x,
		                 y: mouseInput.y);

		thirdPersonLook.Update(input: mouseInput,
		                       look: mouseLook);
		
		crouch.Update(capsule: capsule);
		
		sway.Update(follower: firstPersonMeshRoot,
		            leader: firstPersonCam);

		cameraBob.Update(target: firstPersonCam,
		                 horizontalSpeedNormal: velocityClamp.horizontalSpeedNormal);

		meshBob.Update(target: firstPersonMeshRoot.parent,
		               horizontalSpeedNormal: velocityClamp.horizontalSpeedNormal);

		player.grounding.Update(rb: rb,
		                        capsule: capsule);
		
		jump.Update(grounded: player.grounding.onTheGround);
	}


	public void FixedUpdate()
	{
		strafeVelocity.FixedUpdate(rb: rb,
		                           input: strafeInput,
		                           grounded: player.grounding.onTheGround);

		jump.FixedUpdate(rb: rb);
		
		velocityClamp.FixedUpdate(rb: rb,
		                          input: strafeInput);
	}

}