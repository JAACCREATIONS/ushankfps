﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHealthy
{

	int CurrentHealth
	{
		get;
		set;
	}
	
	int MaxHealth
	{
		get;
		set;
	}
	
	void TakeDamage(int amount);

}