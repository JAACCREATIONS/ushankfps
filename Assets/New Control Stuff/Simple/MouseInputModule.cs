﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MouseInputModule
{

    public float xSensitivity = 80.0f;
    public float ySensitivity = 80.0f;
    public float wSensitivity = 10.0f;

    public float x;
    public float y;
    public float w;

    private const string xInputName = "Mouse X";
    private const string yInputName = "Mouse Y";
    private const string wInputName = "Mouse ScrollWheel";


    public void Update()
    {
        x = Input.GetAxis(axisName: xInputName) * Time.deltaTime * xSensitivity;
        y = Input.GetAxis(axisName: yInputName) * Time.deltaTime * ySensitivity;
        w = Input.GetAxis(axisName: wInputName) * Time.deltaTime * wSensitivity;
    }

}