﻿using System;

using UnityEngine;

[Serializable]
public class VelocityClampModule
{
	
	[Header(header: "Settings")]
	public float maxHorizontalWalkSpeed = 2.5f;
	public float maxHorizontalRunSpeed  = 7.0f;
	
	[Header(header: "Output")]
	public float horizontalSpeedNormal;

	public void FixedUpdate(Rigidbody rb, StrafeInputModule input)
	{
		// Read velocity from rigidBody

		var v = rb.velocity;

		var y = v.y;

		// Flatten y so our horizontal magnitude clamp doesn't factor in up/down
		v.y = 0.0f; 

		// Clamp horizontal Velocity

		var max = input.runInput ? maxHorizontalRunSpeed : maxHorizontalWalkSpeed;

		v = Vector3.ClampMagnitude(vector: v,
		                           maxLength: max);

		horizontalSpeedNormal = v.magnitude / max;

		// Write velocity back to rigidBody

		rb.velocity = new Vector3(x: v.x,
		                          y: y,
		                          z: v.z);
	}

}