﻿using System;

using UnityEngine;

[Serializable]
public class ThirdPersonLookModule
{

	[Header("References")]
	public Transform thirdPersonRoot;
	public Transform thirdPersonHipBone;

	public float thirdPersonHipPitch;
	public float hipDownClamp = -179.0f;
	public float hipUpClamp   = -1.0f;


	public void Update(MouseInputModule input,
	                   MouseLookModule  look)
	{
		thirdPersonRoot.localEulerAngles = new Vector3(x: 0,
		                                               y: look.yaw,
		                                               z: 0);

		thirdPersonHipPitch = Mathf.Clamp(value: thirdPersonHipPitch - input.y,
		                                  min: hipDownClamp,
		                                  max: hipUpClamp);

		thirdPersonHipBone.localEulerAngles = new Vector3(x: thirdPersonHipPitch,
		                                                  y: 0,
		                                                  z: 0);
	}

}