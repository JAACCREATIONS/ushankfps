﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundEntity : PhysicsEntity
{
	
	[SerializeField]
	public GroundingModule grounding;
	
	public override void OnValidate()
	{
		base.OnValidate();

		rb.freezeRotation = true;
		
		grounding.groundLayerMask = LayerMask.GetMask("ground");
	}

	public virtual void Update() =>
		grounding.Update(rb: rb,
		                 capsule: collider);

	protected void OnDrawGizmosSelected()
	{
		if (!Application.isPlaying) return;
		
		for (var i = 0;
		     i < grounding.groundHits.Length;
		     i++)
		{
			Gizmos.color = grounding.groundHitValidity[i] ? Color.green : Color.red;
			
			Gizmos.DrawSphere(center: grounding.groundHits[i].point, radius: 0.1f);
		}
	}
	
}