﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class StrafeInputModule
{
	
	[Header(header: "Input")]
	public KeyCode forwardsKey = KeyCode.W;
	public KeyCode backwardKey = KeyCode.S;
	public KeyCode leftKey     = KeyCode.A;
	public KeyCode rightKey    = KeyCode.D;

	public KeyCode runKey = KeyCode.LeftShift;

	public bool forwardInput;
	public bool backwardInput;
	public bool leftInput;
	public bool rightInput;

	public bool runInput;

	public  Vector3 inputAxis;
	public  Vector3 smoothInputAxis;
	private Vector3 smoothVelocity;

	public float smoothTime = 0.1f;



	public void Update()
	{
		forwardInput  = Input.GetKey(key: forwardsKey);
		backwardInput = Input.GetKey(key: backwardKey);
		leftInput     = Input.GetKey(key: leftKey);
		rightInput    = Input.GetKey(key: rightKey);

		runInput = Input.GetKey(key: runKey);

		inputAxis = new Vector3(x: (leftInput ? -1.0f : 0.0f) + (rightInput ? 1.0f : 0.0f),
		                        y: 0.0f,
		                        z: (backwardInput ? -1.0f : 0.0f) + (forwardInput ? 1.0f : 0.0f)).normalized;

		smoothInputAxis = Vector3.SmoothDamp(current: smoothInputAxis,
		                                     target: inputAxis,
		                                     currentVelocity: ref smoothVelocity,
		                                     smoothTime: smoothTime);
	}

}