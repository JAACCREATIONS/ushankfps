﻿using System;

using UnityEngine;

[Serializable]
public class SwayModule
{

	[Header(header: "Sway Time")]
	[SerializeField]
	public float swayTime = 0.028f;

	public void Update(Transform follower, Transform leader)
	{
		var f = follower.forward;

		var v = Vector3.zero;

		f = Vector3.SmoothDamp(current: f,
		                       target: leader.forward,
		                       currentVelocity: ref v,
		                       smoothTime: swayTime);

		follower.forward = f;
	}

}
