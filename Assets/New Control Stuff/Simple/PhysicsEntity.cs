﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public abstract class PhysicsEntity : MonoBehaviour
{

	public Transform root;
	
	public Rigidbody       rb;
	public CapsuleCollider collider;


	public virtual void OnValidate()
	{
		root     = transform;
		rb       = GetComponentInChildren<Rigidbody>();
		collider = GetComponentInChildren<CapsuleCollider>();
	}
	
}