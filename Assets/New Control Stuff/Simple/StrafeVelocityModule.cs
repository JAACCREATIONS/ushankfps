﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class StrafeVelocityModule
{

	[Header(header: "Velocity Setting")]
	[SerializeField]
	public Transform camTrans;

	[Header(header: "Settings")]
	public float groundAccel = 80.0f;
	public float airAccel    = 20.0f;


	public virtual void FixedUpdate(Rigidbody         rb,
	                                StrafeInputModule input,
	                                bool              grounded)
	{
		var currentAccel = grounded ? groundAccel : airAccel;

		// ----- Apply Horizontal Force To RigidBody

		var camRight = camTrans.right;

		camRight.y = 0.0f;

		camRight.Normalize();

		rb.AddForce(force: camRight * (input.smoothInputAxis.x * currentAccel)); // * Time.fixedDeltaTime..?

		var camFwd = camTrans.forward;

		camFwd.y = 0.0f;

		camFwd.Normalize();

		rb.AddForce(force: camFwd * (input.smoothInputAxis.z * currentAccel)); // * Time.fixedDeltaTime..?

	}

}
