﻿using System;

using UnityEngine;

[Serializable]
public class JumpModule
{
	
	[SerializeField]
	public KeyCode jumpKey = KeyCode.Space;

	[SerializeField]
    public bool jumpFrame;
    [SerializeField]
    public bool jumpRequest;

    [SerializeField]
    public float jumpThrust = 6.0f;
    
    public void Update(bool grounded)
    {
	    jumpFrame = Input.GetKeyDown(key: jumpKey);

	    if (grounded && jumpFrame)
	    {
		    jumpRequest = true;
	    }
    }

    public void FixedUpdate(Rigidbody rb)
    {
	    if (jumpRequest)
	    {
		    jumpRequest = false;

		    var v = rb.velocity;
		    
		    v.y         = jumpThrust;
		    
		    rb.velocity = v;
	    }
    }

}
