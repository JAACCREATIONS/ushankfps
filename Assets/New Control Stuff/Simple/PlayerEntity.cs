﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEntity : GroundEntity
{
    public static PlayerEntity i;

    public EntityController controls;
    
    protected void Awake()
    {
        i = this;
    }
}