﻿using System;

using UnityEngine;

[Serializable]
public class MouseLookModule
{

	[Header(header: "Horizontal")] [SerializeField]
	public float yaw;

	[Header(header: "Vertical")] [SerializeField]
	public float pitch;

	[SerializeField]
	public float downClamp = -89.0f;
	[SerializeField]
	public float upClamp = 89.0f;


	public void Update(Transform camTrans,
	                   float x,
	                   float y)
	{
		yaw += x;

		pitch = Mathf.Clamp(value: pitch - y,
		                    min: downClamp,
		                    max: upClamp);

		camTrans.localEulerAngles = new Vector3(x: pitch,
		                                        y: yaw,
		                                        z: 0.0f);
	}

}