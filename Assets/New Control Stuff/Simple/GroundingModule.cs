﻿using System;

using UnityEngine;

[Serializable]
public class GroundingModule
{

	[Header(header: "References")]
	[SerializeField]
	public Transform       root;

	[Header(header: "Detection")]
	[SerializeField]
	public LayerMask groundLayerMask;

	public const int hitTrackCount = 4;

	public RaycastHit[] groundHits = new RaycastHit[hitTrackCount];
	
	[SerializeField]
	public bool[] groundHitValidity = new bool[hitTrackCount];

	[SerializeField]
	public float minimumGroundNormalAngle = 0.75f;
	
	[Header(header: "Grounded Settings")]
	[SerializeField]
	[Tooltip(tooltip: "How long should the ground-check ray be while we're already on the ground?")]
	public float groundRayGroundLength = 0.25f;
	[SerializeField]
	public float groundRayRadius = 0.25f;
	[SerializeField]
	public float groundDrag = 8.0f;

	[Header(header: "Mid-air Settings")]
	[SerializeField]
	[Tooltip(tooltip: "How long should the ground-check ray be while we're in the air?")]
	public float groundRayAirLength    = 0.1f;
//	[SerializeField]
//	public float groundRayAirRadius = 0.25f;
	[SerializeField]
	public float airDrag = 0.5f;

//	[Header(header: "Landing")]
//	[SerializeField]
//	public float groundSnapOffset      = 0.05f;

	[Header(header: "Output")]
	[SerializeField]
	public bool onTheGround = false;

//	public float hoverHeight     = 0.25f;
//	public float hoverSnapMargin = 0.1f;
	
//	public float hackRadius;
//	public float hackLength;
	
	public void Update(Rigidbody rb, CapsuleCollider capsule)
	{
		for (var i = 0;
		     i < hitTrackCount;
		     i++)
		{
			groundHits[i]        = default;
			groundHitValidity[i] = false;
		}

		Physics.SphereCastNonAlloc(origin: root.position,
		                           // If we're already on the ground, its beneficial to have a bit of extra ground-ray length to really secure the grounding.
		                           // But if we're mid-air, having that extra length can create an ugly snap when you land. We want it short in mid-air basically.
		                           radius: groundRayRadius,
		                           direction: Vector3.down,
		                           results: groundHits,
		                           maxDistance: capsule.height * 0.5f + (onTheGround ? groundRayGroundLength : groundRayAirLength),
		                           layerMask: groundLayerMask);

		// Find the tallest valid hit, if any
		var tallestHitHeight   = float.MinValue;
		var atLeastOneValidHit = false;

		for (var i = 0;
		     i < hitTrackCount;
		     i++)
		{
			var h = groundHits[i];

			var valid = h.collider && h.normal.y > minimumGroundNormalAngle;

			groundHitValidity[i] = valid;

			if (valid)
			{
				var y = h.point.y;

				if (y > tallestHitHeight)
				{
//					Debug.Log($"The SphereCast hit [{h.collider.name}]! It's normal y is [{h.normal.y}]");

					atLeastOneValidHit = true;

					tallestHitHeight = y;
				}
			}

		}

		// If there was a valid hit, snap to the tallest point we found.
		if (atLeastOneValidHit)
		{
			var p = root.position;

//			p.y = tallestHitHeight + (capsule.height * 0.5f + groundSnapOffset);
			p.y = tallestHitHeight + (capsule.height * 0.5f + groundRayGroundLength);

			root.position = p;
		}

		// Finally, atLeastOneValidHit = we're on the ground
		
		// But has this changed since last frame?
		
		if (onTheGround == atLeastOneValidHit) return;

		onTheGround = atLeastOneValidHit;

		if (onTheGround)
		{
			rb.useGravity = false;

			var v = rb.velocity;

			v.y = 0.0f;

			rb.velocity = v;

			rb.drag = groundDrag;
		}
		else
		{
			rb.useGravity = true;

			rb.drag = airDrag;
		}
	}

	
}