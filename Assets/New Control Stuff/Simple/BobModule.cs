﻿using System;

using UnityEngine;

[Serializable]
public class BobModule
{
	[Header(header: "Settings")]
	[SerializeField]
	public float bobHeightOffset = 0.0f;
    
	[SerializeField]
	public float bobRate = 16.0f;

	[SerializeField]
	public float bobStrength = 0.025f;

	[Header(header: "Output")]
	[SerializeField]
	public float bob;

	public void Update(Transform target,
	                   float horizontalSpeedNormal)
	{
		var bobScale = Mathf.Lerp(a: 0.0f,
		                 b: bobStrength,
		                 t: horizontalSpeedNormal);

		bob = Mathf.Sin(f: Time.time * bobRate) * bobScale;

		var p = target.localPosition;

		p.y = bobHeightOffset + bob;

		target.localPosition = p;
	}
}