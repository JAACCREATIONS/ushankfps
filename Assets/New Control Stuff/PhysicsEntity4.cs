﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsEntity4 : ManualVelocityBase
{

	public float minimumGroundNormalAngle = 0.5f;
	public float currentGroundNormalAngle;
	
	public int groundHitCount;

	public const int hitTrackCount = 4;
	
	public RaycastHit[] groundHits = new RaycastHit[hitTrackCount];

//	public Vector3[] validGroundHits = new Vector3[hitTrackCount];
//	public Vector3[] invalidGroundHits = new Vector3[hitTrackCount];

	public bool[] groundHitValidity = new bool[hitTrackCount];

	public override void LeftTheGround()
	{
		base.LeftTheGround();

		for (var i = 0;
		     i < groundHits.Length;
		     i++)
		{
			groundHits[i] = new RaycastHit();
		}
	}

	public override void Update()
	{
		base.Update();

		for (var i = 0;
		     i < hitTrackCount;
		     i++)
		{
			groundHits[i]        = new RaycastHit();
			groundHitValidity[i] = false;

//			validGroundHits[i] = Vector3.zero;
//			invalidGroundHits[i] = Vector3.zero;
		}
		
//		var onTheGroundTemp = false;

		groundHitCount = Physics.SphereCastNonAlloc(origin: root.position,
		                                            radius: collider.radius + 0.1f,
		                                            direction: Vector3.down,
		                                            results: groundHits,
		                                            maxDistance: DefaultRayLength,
		                                            layerMask: groundLayerMask);


//		for (var i = 0;
//		     i < groundHitValidity.Length;
//		     i++)
//		{
//			if (groundHitValidity[i])
//			{
//				onTheGroundTemp = true;
//
//				break;
//			}
//		}


//		if (onTheGroundTemp)
//		{
////			SnapToHighestGroundHitPoint();
//			
//			BetterSnapToHighGroundHit();
//		}

		var tallestHitHeight   = float.MinValue;
		var atLeastOneValidHit = false;
		
		for (var i = 0;
		     i < hitTrackCount;
		     i++)
		{
			var h = groundHits[i];
				
			var valid = h.collider && h.normal.y > minimumGroundNormalAngle;

			groundHitValidity[i] = valid;

			if (valid)
			{
				var y = h.point.y;
				
				if (y > tallestHitHeight)
				{
					atLeastOneValidHit = true;

					tallestHitHeight = y;
				}
			}

		}
		
		if (atLeastOneValidHit)
		{
			var p = root.position;

			p.y = tallestHitHeight + DefaultRayLength;

			root.position = p;
		}

		
		onTheGround = atLeastOneValidHit;
	}


//	bool BetterSnapToHighGroundHit()
//	{
//		var tallestHitHeight   = float.MinValue;
//		var atLeastOneValidHit = false;
//		
//		for (var i = 0;
//		     i < hitTrackCount;
//		     i++)
//		{
//			var h = groundHits[i];
//				
//			var valid = h.collider && h.normal.y > minimumGroundNormalAngle;
//
//			groundHitValidity[i] = valid;
//
//			if (valid)
//			{
//				var y = h.point.y;
//				
//				if (y > tallestHitHeight)
//				{
//					atLeastOneValidHit = true;
//
//					tallestHitHeight = y;
//				}
//			}
//
//		}
//		
//		if (atLeastOneValidHit)
//		{
//			var p = root.position;
//
//			p.y = tallestHitHeight + RayHeight;
//
//			root.position = p;
//		}
//	}
//
//	void SnapToSingularGroundHitPoint()
//	{
//		var p = root.position;
//
//		p.y = groundHits[1].point.y + RayHeight;
//				
//		root.position = p;
//	}
//	
//	void SnapToHighestGroundHitPoint()
//	{
//		var highestP            = float.MinValue;
//		var snappablePointFound = false;
//
//		for (var i = 0;
//		     i < groundHitCount;
//		     i++)
//		{
//			var h = groundHits[i];
//
////			if (h.collider) // Is this hit populated?
////			{
//				currentGroundNormalAngle = h.normal.y;
//				
//				if (currentGroundNormalAngle > minimumGroundNormalAngle) // Does its normal meet our minimum angle requirement?
//				{
//					var y = h.point.y;
//
//					if (y > highestP) // Is it the tallest point out of our hits? If we don't check this, we might accidentally snap to a lower hit (like the ground under a step)
//					{
//						snappablePointFound = true;
//						highestP            = y;
//					}
//				}
////			}
//		}
//
//		if (snappablePointFound)
//		{
//			var p = root.position;
//
//			p.y = highestP + RayHeight;
//
//			root.position = p;
//		}
//	}

	public override void FixedUpdate()
	{
		base.FixedUpdate();
		
		
		
		ApplyVelocity();
	}


	protected override void OnDrawGizmosSelected()
	{
		if (!Application.isPlaying) return;
		
		base.OnDrawGizmosSelected();

		for (var i = 0;
		     i < groundHits.Length;
		     i++)
		{
			Gizmos.color = groundHitValidity[i] ? Color.green : Color.red;
			
			Gizmos.DrawSphere(center: groundHits[i].point, radius: 0.1f);
		}

//		Gizmos.color = Color.green;
		
//		foreach (var h in groundHits)
//		{
//			Gizmos.DrawSphere(h.point, 0.1f);
//		}
		
	}

}