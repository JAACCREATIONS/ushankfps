﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticVelocityBase : PhysicsEntityBase
{

	public KeyCode runKey = KeyCode.LeftShift;
	public bool    runInput;

	public Vector3 horizontalSpeed;
	
	public float maxHorizontalWalkSpeed = 5.0f;
	public float maxHorizontalRunSpeed  = 10.0f;

	public float horizontalSpeedNormal = 0.0f;

	public float verticalSpeed;
	
	public float maxVerticalSpeed = 10.0f;
	
	public float groundAccel = 130.0f;
	public float airAccel = 40.0f;

	public float groundDrag = 8.0f;
	public float airDrag    = 0.5f;

	[Header("Jump")]
	public float jumpThrust;
	public float jumpThrustDefault = 1.0f;
	public float jumpThrustDecay   = 0.25f;
	public bool  jumpHeld          = false;
	
	public override void HitTheGround()
	{
		base.HitTheGround();
		
		rb.drag       = groundDrag;
	}


	public override void LeftTheGround()
	{
		base.LeftTheGround();

		rb.drag = airDrag;
	}


	public override void Update()
	{
		base.Update();

		runInput = Input.GetKey(runKey);
	}


	protected override void JumpKeyDown()
	{
		if (onTheGround)
		{
			jumpThrust = jumpThrustDefault;

//			Debug.Log($"[{Time.frameCount}] Jump! JumpThrust is now [{jumpThrust}]");
			
			jumpHeld = true;
		}
	}


	protected override void JumpKeyReleased()
	{
		jumpHeld = false;

//		jumpThrust = 0.0f;
	}


	protected virtual void FixedUpdate()
	{
		ApplyHorizontalForceToRigidBody();
		
		ReadVelocityFromRigidBody();
		
		ClampHorizontalVelocity();

		HandleJumping();
		
		ClampVerticalSpeed();

		WriteVelocityToRigidBody();
	}


	public void ApplyHorizontalForceToRigidBody()
	{
//		rb.AddForce(force: smoothInputAxis * CurrentAccel);

//		rb.AddForce(force: new Vector3(x: LocalRight.x * smoothInputAxis.x * CurrentAccel,
//		                               y: 0,
//		                               z: LocalForward.z * smoothInputAxis.z * CurrentAccel));

//		rb.AddForce(force: new Vector3(x: (CamRight.x + CamForward.x) * smoothInputAxis.x * CurrentAccel,
//		                               y: 0,
//		                               z: (CamRight.z + CamForward.z) * smoothInputAxis.z * CurrentAccel));

		var camRight = CamRight;

		camRight.y = 0.0f;
		
		camRight.Normalize();

		rb.AddForce(camRight   * (smoothInputAxis.x * CurrentAccel)); // * Time.fixedDeltaTime..?
		
		var camFwd = CamForward;

		camFwd.y = 0.0f;
		
		camFwd.Normalize();
		
		rb.AddForce(camFwd * (smoothInputAxis.z * CurrentAccel)); // * Time.fixedDeltaTime..?
	}
	
	public void ReadVelocityFromRigidBody()
	{
		horizontalSpeed = rb.velocity;
		
		verticalSpeed = horizontalSpeed.y;
		
		horizontalSpeed.y = 0.0f; // Flatten y so our clamp is only working on horizontal velocity
	}

	public void ClampHorizontalVelocity()
	{
		var max = CurrentMaxHorizontalSpeed;

		horizontalSpeed = Vector3.ClampMagnitude(vector: horizontalSpeed, // If they're mid-air, limit their horizontal speed to the maximum walking pace
		                                         maxLength: max);

		horizontalSpeedNormal = horizontalSpeed.magnitude / max;
	}


	public void HandleJumping()
	{
		if (jumpInput)
		{
			verticalSpeed += jumpThrust;
		}

		if (jumpThrust > 0.0)
		{
			jumpThrust -= jumpThrustDecay;
		}
	}

	public void ClampVerticalSpeed()
	{
		verticalSpeed = Mathf.Clamp(value: verticalSpeed,
		                            min: -maxVerticalSpeed,
		                            max: maxVerticalSpeed);
	}


	protected void WriteVelocityToRigidBody()
	{
		rb.velocity = new Vector3(x: horizontalSpeed.x,
		                          y: verticalSpeed,
		                          z: horizontalSpeed.z);
	}
	

	float CurrentAccel => onTheGround ? groundAccel : airAccel;

	protected float CurrentMaxHorizontalSpeed =>
//		onTheGround ? runInput ? maxHorizontalRunSpeed : maxHorizontalWalkSpeed : maxHorizontalWalkSpeed;
		runInput ? maxHorizontalRunSpeed : maxHorizontalWalkSpeed;

}
