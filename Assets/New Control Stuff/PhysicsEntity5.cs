﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsEntity5 : AutomaticVelocityBase
{

//	public float rayOffsetMin = 0.1f;
//	public float rayOffsetMax = 0.25f;

//	public float dynamicRayHeight = 1.0f;

	public float minimumGroundNormalAngle = 0.5f;

	public const int hitTrackCount = 4;

	public RaycastHit[] groundHits = new RaycastHit[hitTrackCount];

	public bool[] groundHitValidity = new bool[hitTrackCount];


	void Awake()
	{
		Application.targetFrameRate = 144;
	}
	
	public override void Update()
	{
		base.Update();

//		if (verticalSpeed <= 0.01f) // This was causing heaps of ground-detection bugs!! You could walk on air etc
//		{
//			CheckForGround();
//		}

//		if (jumpThrust <= 0.0f)
//		{
//			CheckForGround();
//		}

//		if (!jumpHeld)
		if (jumpThrust < 0.01f)
		{
//			Debug.Log($"[{Time.frameCount}] Checking for ground");

			CheckForGround();
		}
	}


	private void CheckForGround()
	{
//		var output = "Emptying hits -";
		
		for (var i = 0;
		     i < hitTrackCount;
		     i++)
		{
//			output               += " " + i;
			groundHits[i]        =  new RaycastHit();
			groundHitValidity[i] =  false;
		}

//		Debug.Log(output);
		
		Physics.SphereCastNonAlloc(origin: root.position,
//		                           radius: collider.radius + 0.1f, // This works great! Just testing without...
//		                           radius: collider.radius + DynamicGroundRayRadius, // So does this...
		                           radius: DynamicGroundRayRadius,
		                           direction: Vector3.down,
		                           results: groundHits,
		                           maxDistance: DefaultRayLength + DynamicGroundRayLength,
		                           layerMask: groundLayerMask);

		// Find the tallest valid hit, if any
		var tallestHitHeight   = float.MinValue;
		var atLeastOneValidHit = false;

		for (var i = 0;
		     i < hitTrackCount;
		     i++)
		{
			var h = groundHits[i];

			var valid = h.collider && h.normal.y > minimumGroundNormalAngle;

			groundHitValidity[i] = valid;

			if (valid)
			{
				var y = h.point.y;

				if (y > tallestHitHeight)
				{
//					Debug.Log($"The SphereCast hit [{h.collider.name}]! It's normal y is [{h.normal.y}]");

					atLeastOneValidHit = true;

					tallestHitHeight = y;
				}
			}

		}

		// If there was a valid hit, snap to the tallest point we found.
		if (atLeastOneValidHit)
		{
			var p = root.position;

			p.y = tallestHitHeight + GroundSnapHeight;

			root.position = p;
		}

		// Finally, can we be considered grounded?
		onTheGround = atLeastOneValidHit;
	}


	protected override void FixedUpdate()
	{
		base.FixedUpdate();
		
//		dynamicRayHeight = GetDynamicRayHeight();
	}


//	protected float GetDynamicRayHeight() =>
//		collider.height * 0.5f +
//		Mathf.Lerp(a: rayOffsetMin,
//		           b: rayOffsetMax,
//		           t: horizontalSpeedNormal);
	    
    protected override void OnDrawGizmosSelected()
	{
		if (!Application.isPlaying) return;
		
		base.OnDrawGizmosSelected();

		for (var i = 0;
		     i < groundHits.Length;
		     i++)
		{
			Gizmos.color = groundHitValidity[i] ? Color.green : Color.red;
			
			Gizmos.DrawSphere(center: groundHits[i].point, radius: 0.1f);
		}
	}

}
