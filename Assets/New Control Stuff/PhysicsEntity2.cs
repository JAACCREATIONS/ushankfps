﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(requiredComponent: typeof(Rigidbody), requiredComponent2: typeof(CapsuleCollider))]
[Serializable]
public class PhysicsEntity2 : MonoBehaviour
{

	public Rigidbody       rb;
	public CapsuleCollider collider;
	
	public Transform root;
	public Transform center;

	public Vector3 lastGroundHit;
	
//	public float height       = 1.5f;
	public const float heightOffset = 0.0001f;

//	public float debugHeight = 1.0f;

	public float groundDrag = 8.0f;
	public float midAirDrag = 0.4f;
	
	public LayerMask groundLayerMask;

	public int groundHitCount;

	public RaycastHit[] groundHits = new RaycastHit[4];

	public Collider[] groundColliders = new Collider[4];

	public float[] groundDistances = new float[4];

	public Vector3[] groundPoints = new Vector3[4];
	
	[SerializeField]
	private bool _onTheGround = false;
	public bool onTheGround
	{
		get => _onTheGround;
		set
		{
			if (_onTheGround == value) return;

			_onTheGround = value;

			if (_onTheGround)
				HitTheGround();
			else
				LeftTheGround();
		}
	}

	public float groundSpeed = 130.0f;

	public float airSpeed = 40.0f;

	public float maxHorizontalSpeed = 5.0f;
	public float maxVerticalSpeed   = 10.0f;
	
	public bool forward;
	public bool backward;
	public bool left;
	public bool right;

//	public float forwardAxis;
//	public float horizontalAxis;
	
	void OnValidate()
	{

		if (groundLayerMask.value == 0)
		{
			groundLayerMask = LayerMask.GetMask("ground");
		}

		root     = transform;
		rb       = GetComponent<Rigidbody>();
		collider = GetComponent<CapsuleCollider>();

		groundDrag = rb.drag;
		
		if (!center)
		{
			for (var i = 0;
			     i < root.childCount;
			     i++)
			{
				if (!string.Equals(a: root.GetChild(i).name,
				                   b: "Center")) continue;

				center = root.GetChild(i);

				return;
			}

			center = new GameObject("Center").transform;

			center.SetParent(transform);
		}
		
		center.localRotation = Quaternion.identity;
		center.localPosition = Vector3.zero;
		
//			center.localPosition = Vector3.up * (RayHeight);
//		center.localPosition = Vector3.up * (height * 0.5f);
	}


	void Update()
	{
		Debug.DrawRay(start: center.position,
		              dir: Vector3.down * (RayHeight),
		              color: onTheGround ? Color.green : Color.red);

		forward = Input.GetKey(KeyCode.W);
		backward = Input.GetKey(KeyCode.S);
		left = Input.GetKey(KeyCode.A);
		right = Input.GetKey(KeyCode.D);
	}


	void FixedUpdate()
	{
//		var hitGround = Physics.Raycast(origin: center.position,
//		                                direction: Vector3.down,
//		                                hitInfo: out var hit,
//		                                maxDistance: RayHeight,
//		                                layerMask: groundLayerMask);

		groundHitCount = Physics.SphereCastNonAlloc(origin: center.position,
		                                            radius: collider.radius - 0.01f,
		                                            direction: Vector3.down,
		                                            results: groundHits,
		                                            maxDistance: RayHeight,
		                                            layerMask: groundLayerMask);

		for (var i = 0;
		     i < groundHits.Length;
		     i++)
		{
			var h = groundHits[i];

			groundColliders[i] = h.collider;
			groundDistances[i] = h.distance;
			groundPoints[i] = h.point;
		}

		var hitGround = groundHitCount > 0;

		// Set the groundPoint before using it!
		if (hitGround)
		{
			lastGroundHit = groundHits[0].point;
			
//			root.position = lastGroundHit + 
//			                new Vector3(x: 0,
//			                            y: RayHeight,
//			                            z: 0);


		}

        onTheGround = hitGround;
        
        if (forward) MoveForward();
        if (backward) MoveBackward();
        if (left) MoveLeft();
        if (right) MoveRight();

        var v = rb.velocity;

        var ySpeed = v.y;

        // By flattening y before the clamp, we ensure that the horizontal axes (x and z) will be allowed to
        // potentially stretch out to maxHorizontalSpeed.
        v.y = 0.0f;

        v = Vector3.ClampMagnitude(vector: v,
                                   maxLength: maxHorizontalSpeed);

        ySpeed = Mathf.Clamp(value: ySpeed,
                             min: -maxVerticalSpeed,
                             max: maxVerticalSpeed);

        v.y = ySpeed;
        
        // Ensure the rigidbody never goes beyond a set speed
//        rb.velocity = Vector3.ClampMagnitude(vector: rb.velocity,
//                                             maxLength: maxSpeed);

        rb.velocity = v;
	}


    void HitTheGround()
    {
//	    Debug.Log("Hit the ground!");
	    
	    rb.useGravity = false;
	    rb.drag       = groundDrag;

	    var v = rb.velocity;

	    v.y = 0;

	    rb.velocity = v;
	    
	    root.position += new Vector3(x: 0,
	                                 y: RayHeight - groundHits[0].distance,
	                                 z: 0);

//	    var p = root.position;
//
//	    p.y = RayHeight;
//
//	    root.position = p;
	    
//	    rb.WakeUp();
    }


    void LeftTheGround()
    {
//	    Debug.Log("Left the ground!");

	    rb.useGravity = true;
//	    groundDrag    = rb.drag;
	    rb.drag       = midAirDrag;
	    
	    rb.WakeUp();
    }


    private void OnDrawGizmosSelected()
    {
	    if (!Application.isPlaying) return;
	    
//	    Gizmos.color = Color.white;
//
//	    Gizmos.DrawSphere(center: lastGroundHit,
//	                      radius: 0.25f);

//	    Gizmos.DrawLine(from: lastGroundHit,
//	                    to: lastGroundHit +
//	                        new Vector3(x: 0,
//	                                    y: RayHeight,
//	                                    z: 0));

	    Gizmos.color = Color.blue;

	    foreach (var h in groundHits)
	    {
		    if (h.collider)
		    {
			    Gizmos.DrawSphere(center: h.point,
			                      radius: 0.1f);
		    }
	    }

    }

    // Move / Strafe

    void MoveForward() => rb.AddForce(ForwardSpeed);

    void MoveBackward() => rb.AddForce(BackwardSpeed);

    void MoveLeft() => rb.AddForce(LeftSpeed);

    void MoveRight() => rb.AddForce(RightSpeed);

    // Turn

    void Turn(float degrees)
    {
	    var e = root.localEulerAngles;

	    e.y += degrees;

	    root.localEulerAngles = e;
    }

    // Shortcuts

    private float RayHeight => collider.height * 0.5f + heightOffset;
	
    float Speed => onTheGround ? groundSpeed : airSpeed;

    Vector3 Forward => root.forward;

    Vector3 Backward => -root.forward;

    Vector3 Left => -root.right;

    Vector3 Right => root.right;

    Vector3 ForwardSpeed => Forward * Speed;

    Vector3 BackwardSpeed => Backward * Speed;

    Vector3 LeftSpeed => Left * Speed;

    Vector3 RightSpeed => Right * Speed;
}
