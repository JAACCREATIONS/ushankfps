﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwayDemo : MonoBehaviour
{

	[Header("References")]
	
    public Transform camera;

    public Transform firstPersonMeshRoot;

    [Header("Mouse Look")]

    public float xSensitivity = 1.0f;
    public float ySensitivity = 1.0f;

    public float downClamp = -89.0f;
    public float upClamp   = 89.0f;

    [Header("Weapon Sway")]

    public float swayTime = 0.3f;

    [Header("Head Bob")]
    
    public KeyCode testHeadBobKey = KeyCode.W;

    public float bobRate = 1.0f;

    public float bobStrength = 1.0f;

    public float bob;

    public float bobSin;

    [Header("Basic Movement")]

    private float accel;
    
    public float accelRate = 4.0f;

    public float targetSpeed = 4.0f;
    
    void Update()
    {
	    // ------------------------------------------------------------------------------------------- Rotate Camera //

        var x = Input.GetAxis("Mouse X") * Time.deltaTime * xSensitivity;
        var y = Input.GetAxis("Mouse Y") * Time.deltaTime * ySensitivity;

	    camera.Rotate(axis: Vector3.up,
	                  angle: x);

	    var e = camera.localEulerAngles;
	    
	    e.x -= y * ySensitivity;

	    e.z = 0.0f;

	    camera.localEulerAngles = e;
	    
	    // -------------------------------------------------------------------------------- Rotate First Person Mesh //

	    var f = firstPersonMeshRoot.forward;
	    
	    var v = Vector3.zero;

	    f = Vector3.SmoothDamp(current: f,
	                           target: camera.transform.forward,
	                           currentVelocity: ref v,
	                           smoothTime: swayTime);

	    firstPersonMeshRoot.forward = f;
	    
	    // ------------------------------------------------------------------------------------------------ Head Bob //

	    var bobInput = Input.GetKey(testHeadBobKey);

	    bob = Mathf.Lerp(a: bob,
	                     b: bobInput ? bobStrength : 0.0f,
	                     t: Time.deltaTime * 2.0f);
	    
	    bobSin = Mathf.Sin(Time.time * bobRate) * bob;

	    var p = camera.transform.localPosition;

	    p.y = bobSin;

	    camera.transform.localPosition = p;

	    // ------------------------------------------------------------------------------ Crappy Movement For Effect //

	    if (bobInput)
	    {
		    accel = Mathf.Lerp(a: accel,
		                       b: targetSpeed,
		                       t: Time.deltaTime * accelRate);
	    }
	    else
	    {
		    accel = Mathf.Lerp(a: accel,
		                       b: 0,
		                       t: Time.deltaTime * accelRate);
	    }
	    
	    var dir = camera.forward;

	    dir.y = 0.0f;
		    
	    dir.Normalize();

	    transform.root.Translate(translation: dir * accel,
	                             relativeTo: Space.World);

    }
}
