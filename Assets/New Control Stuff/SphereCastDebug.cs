﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereCastDebug : MonoBehaviour
{

	public float sphereCastOffset = 0.00001f;

    public float length = 2.0f;
    public float radius = 1.0f;

    public LayerMask mask;

    public RaycastHit[] hits = new RaycastHit[8];
    
    
    void Update()
    {
	    var hitCount = Physics.SphereCastNonAlloc(origin: transform.position,
	                                              radius: radius,
	                                              direction: transform.forward,
	                                              results: hits,
	                                              maxDistance: length,
	                                              layerMask: mask);

	    if (hitCount == 0)
	    {
		    hits = new RaycastHit[8];
	    }
    }


    private void OnDrawGizmos()
    {
	    if (Application.isPlaying)
	    {
		    foreach (var h in hits)
		    {
			    if (h.collider)
			    {
				    Gizmos.DrawSphere(center: h.point, radius: 0.05f);
			    }
		    }
	    }
    }

}
