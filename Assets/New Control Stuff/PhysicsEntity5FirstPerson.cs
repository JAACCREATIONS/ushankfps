﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class PhysicsEntity5FirstPerson : MonoBehaviour
{

	[Header("References")]

	public Transform root;
	
    public Transform camera;

    public Transform firstPersonMeshRoot;

    public Transform bobTransform;
    
    [Header("Mouse Look")]

    public float xSensitivity = 1.0f;
    public float ySensitivity = 1.0f;

    public float yaw;
    public float pitch;
    
    public float xToClamp;
    
    public float downClamp = -89.0f;
    public float upClamp   = 89.0f;

    [Header("Weapon Sway")]

    public float swayTime = 0.3f;

    [Header("Bobbing")] 
    
    public BobModule cameraBob;

    public BobModule meshBob;
    
    [Serializable]
    public class BobModule
    {
	    [SerializeField]
	    public PhysicsEntity5 entity;
	    
	    [SerializeField]
	    public Transform target;
	    
	    [SerializeField]
	    public float bobHeight = 0.65f;
    
	    [SerializeField]
	    public float bobRate = 16.0f;

	    [SerializeField]
	    public float bobStrength = 0.025f;

	    [SerializeField]
	    public float bob;

	    [SerializeField]
	    public float bobSin;

	    public void DoBob()
	    {
		    bob = Mathf.Lerp(a: 0.0f,
			    b: bobStrength,
			    t: entity.horizontalSpeedNormal);
	    
		    bobSin = Mathf.Sin(Time.time * bobRate) * bob;

		    var p = target.localPosition;

		    p.y = bobHeight + bobSin;

		    target.localPosition = p;
	    }
    }
    



    public PhysicsEntity5 entity;

    [Header("Third Person Mesh Stuff")]
    public Transform thirdPersonRoot;
    public Transform thirdPersonHipBone;

    public float thirdPersonHipPitch;
    public float hipDownClamp = -179.0f;
    public float hipUpClamp   = -1.0f;
    
//    [Header("Basic Movement")]

//    private float accel;
    
//    public float accelRate = 4.0f;

//    public float targetSpeed = 4.0f;

//	void Awake()
//	{
//		bobHeight = bobTransform.localPosition.y;
//	}

    void Update()
    {
	    // ------------------------------------------------------------------------------------------- Rotate Camera //

        var x = Input.GetAxis("Mouse X") * Time.deltaTime * xSensitivity;
        var y = Input.GetAxis("Mouse Y") * Time.deltaTime * ySensitivity;

        yaw   += x;
//        pitch -= y;

        pitch = Mathf.Clamp(value: pitch - y,
                            min: downClamp,
                            max: upClamp);



        camera.localEulerAngles = new Vector3(x: pitch,
                                              y: yaw,
                                              z: 0.0f);

        // ------------------------------------------------------------------------------- Third Person Hip Movement //

        thirdPersonRoot.localEulerAngles = new Vector3(x: 0,
                                                       y: yaw,
                                                       z: 0);

        thirdPersonHipPitch = Mathf.Clamp(value: thirdPersonHipPitch - y,
                                          min: hipDownClamp,
                                          max: hipUpClamp);
        
        thirdPersonHipBone.localEulerAngles = new Vector3(x: thirdPersonHipPitch,
                                                          y: 0,
                                                          z: 0);
        
//	    camera.Rotate(axis: Vector3.up,
//	                  angle: x);

	    
	    
//	    var e = camera.localEulerAngles;
	    
//	    e.x -= y;

//	    e.z = 0.0f;

//	    e.x = Mathf.Clamp(e.x,
//	                      downClamp,
//	                      upClamp);
	    
//	    camera.localEulerAngles = e;
	    
//	    camera.
	    
		// --------------------------------------------------------------------------------------------- Rotate Root //

//		var camFwd = camera.forward;
//
//		camFwd.y = 0.0f;
//
//		root.forward = camFwd;

//	    root.Rotate(axis: Vector3.up,
//	                angle: x);

	    // -------------------------------------------------------------------------------- Rotate First Person Mesh //

	    var f = firstPersonMeshRoot.forward;
	    
	    var v = Vector3.zero;

	    f = Vector3.SmoothDamp(current: f,
	                           target: camera.transform.forward,
	                           currentVelocity: ref v,
	                           smoothTime: swayTime);

	    firstPersonMeshRoot.forward = f;
	    
	    // ------------------------------------------------------------------------------------------------ Head Bob //

	    cameraBob.DoBob();
	    
	    meshBob.DoBob();
	    
//	    var bobInput = Input.GetKey(testHeadBobKey);

	    // Classic bob (works great)
//	    bob = Mathf.Lerp(a: bob,
//	                     b: bobInput ? bobStrength : 0.0f,
//	                     t: Time.deltaTime * 4.0f);
	    
		// This version should be attached to your horizontal speed normal?


	    // ------------------------------------------------------------------------------ Crappy Movement For Effect //

//	    if (bobInput)
//	    {
//		    accel = Mathf.Lerp(a: accel,
//		                       b: targetSpeed,
//		                       t: Time.deltaTime * accelRate);
//	    }
//	    else
//	    {
//		    accel = Mathf.Lerp(a: accel,
//		                       b: 0,
//		                       t: Time.deltaTime * accelRate);
//	    }
//	    
//	    var dir = camera.forward;
//
//	    dir.y = 0.0f;
//		    
//	    dir.Normalize();
//
//	    transform.root.Translate(translation: dir * accel,
//	                             relativeTo: Space.World);

    }
}
