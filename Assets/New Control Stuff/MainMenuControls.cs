﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuControls : MonoBehaviour
{

//    public KeyHandler toggleMainMenuKey;

    public KeyCode toggleMainMenuKey;

    [SerializeField]
    private bool _menuActive = false;
    public bool menuActive
    {
        get => _menuActive;
        set
        {
            if (_menuActive == value) return;

            _menuActive = value;
            
            SetMainMenuMode();
        }
    }

    public GameObject menuobject;

//    void Start() => toggleMainMenuKey.onKeyDown.AddListener(ToggleMainMenu);

    void ToggleMainMenu() => menuActive = !menuActive;

    void SetMainMenuMode()
    {
        if (_menuActive)
        {
            Pause();
        }
        else
        {
            Unpause();
        }
    }

    void Pause()
    {
        Time.timeScale = 0;
        
        Cursor.lockState = CursorLockMode.None;
        
        menuobject.SetActive(true);
        
        BasePlayerControls.CurrentBasePlayerControls.enabled = false;
        
    }

    void Unpause()
    {
        Time.timeScale = 1;
        
        Cursor.lockState = CursorLockMode.Locked;
        
        menuobject.SetActive(false);
        
        BasePlayerControls.CurrentBasePlayerControls.enabled = true;
        
    }

    void Update()
    {
        if (Input.GetKeyDown(toggleMainMenuKey)) ToggleMainMenu();
    }
}