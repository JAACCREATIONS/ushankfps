﻿using UnityEngine;
using System.Collections;

public class SmoothDampTest : MonoBehaviour
{

	public  Transform target;
	public  float     smoothTime = 0.3F;
	public Vector3   velocity   = Vector3.zero;


	void Update()
	{
		// Define a target position above and behind the target transform
		Vector3 targetPosition = target.TransformPoint(new Vector3(x: 0,
		                                                           y: 5,
		                                                           z: -10));

		// Smoothly move the camera towards that target position
		transform.position = Vector3.SmoothDamp(current: transform.position,
		                                        target: targetPosition,
		                                        currentVelocity: ref velocity,
		                                        smoothTime: smoothTime);
	}

}