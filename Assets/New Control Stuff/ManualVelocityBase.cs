﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManualVelocityBase : PhysicsEntityBase
{
	
	[Header("Horizontal Movement")]
	[Range(min: 0.1f, max: 2.0f)]
	public float horizontalAcceleration = 1.0f;
	
	[Range(min: 0.01f, max: 0.1f)]
	public float horizontalDeceleration = 0.03f;

	[Range(min: 1.0f, max: 10.0f)]
	public float maxHorizontalSpeed = 6.0f;

	public Vector3 horizontalMomentum;

	[Header("Vertical Movement")]
	public float verticalMomentum;

	public virtual void FixedUpdate()
	{
		if (inputAxis.sqrMagnitude > Mathf.Epsilon)
		{
			horizontalMomentum += inputAxis * horizontalAcceleration;
		}
		else
		{
			var vel = Vector3.zero;
			
			horizontalMomentum = Vector3.SmoothDamp(current: horizontalMomentum,
			                                        target: Vector3.zero,
			                                        currentVelocity: ref vel,
			                                        smoothTime: horizontalDeceleration);
		}

		horizontalMomentum = Vector3.ClampMagnitude(vector: horizontalMomentum,
		                                            maxLength: maxHorizontalSpeed);
	}


	protected void ApplyVelocity()
	{
		verticalMomentum = rb.velocity.y;
		
		rb.velocity = new Vector3(x: horizontalMomentum.x,
		                          y: verticalMomentum,
		                          z: horizontalMomentum.z);
	}
}
