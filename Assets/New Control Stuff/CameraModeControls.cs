﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;
//
//public class CameraModeControls : MonoBehaviour
//{
////    public KeyHandler swapCameraModeKey;
//
//    [SerializeField]
//    private bool _firstPerson = true;
//    public bool firstPerson
//    {
//        get => _firstPerson;
//        set
//        {
//            if (_firstPerson == value) return;
//
//            _firstPerson = value;
//
//            if (Application.isPlaying) EnterCurrentMode();
//        }
//    }
//
//    private BasePlayerControls firstPersonControls;
//    private BasePlayerControls thirdPersonControls;
//
//    public BasePlayerControls GetCurrentPlayerControls() => _firstPerson ? firstPersonControls : thirdPersonControls;
//    
//    private void OnValidate()
//    {
//        
//    }
//
//    void Awake()
//    {
//        firstPersonControls = GetComponent<FirstPersonControls>();
//        thirdPersonControls = GetComponent<ThirdPersonControls>();
//        
//        swapCameraModeKey.onKeyDown.AddListener(ToggleCameraMode);
//
//        EnterCurrentMode();
//    }
//
//    void ToggleCameraMode() => firstPerson = !firstPerson;
//
//    void EnterCurrentMode()
//    {
//        if (firstPerson)
//        {
//            EnterFirstPersonMode();
//        }
//        else
//        {
//            EnterThirdPersonMode();
//        }
//    }
//
//    void EnterFirstPersonMode()
//    {
//        thirdPersonControls.enabled = false;
//
//        firstPersonControls.enabled = true;
//    }
//
//    void EnterThirdPersonMode()
//    {
//        firstPersonControls.enabled = false;
//        
//        thirdPersonControls.enabled = true;
//    }
//
//    void Update() => swapCameraModeKey.Check();
//}
