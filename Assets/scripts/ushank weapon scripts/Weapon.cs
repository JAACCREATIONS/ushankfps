﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
	public bool pickedUp = false;
	
	protected virtual void OnValidate()
	{
		gameObject.name = GetType().Name.Substring(7, GetType().Name.Length-7);
	}

	public virtual bool CanBeEquipped() 
	{
		return true; 
	}

	public virtual async Task Equip()
	{
		Debug.Log($"[{name}] equipped!");
	}

	public virtual async Task Unequip()
	{
		Debug.Log($"[{name}] unequipped!");
	}
}
