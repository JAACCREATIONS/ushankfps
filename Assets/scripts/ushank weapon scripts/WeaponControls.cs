﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

public class WeaponControls : MonoBehaviour
{

	public static WeaponControls access;

	public List<Weapon> weapons = new List<Weapon>();

	public int currentWeaponIndex = 0;

	public bool canSwapWeapons = true;

	public Weapon currentWeapon;
	
	void Awake()
	{
		access = this;
	}
	
	void Start()
	{
		weapons = GetComponentsInChildren<Weapon>(true).ToList();
		
		EquipCurrent();
	}

	void Update()
	{
		if (canSwapWeapons)
		{
			if (Input.mouseScrollDelta.y < 0)
				CycleDown();
			else if (Input.mouseScrollDelta.y > 0)
				CycleUp();			
		}
	}

	[ContextMenu("Cycle Down While")]
	void CycleDown()
	{
		int nextWeaponIndexToTry = WeaponIndexWrapAround(currentWeaponIndex - 1);

		Debug.Log($"We're going to start looking for the next equippable weapon, starting from the current weapon [{weapons[currentWeaponIndex].name}");

		while (nextWeaponIndexToTry != currentWeaponIndex) // This will stop when we're back where we started from!
		{
			Debug.Log($"Can the [{weapons[nextWeaponIndexToTry]}] be equipped?");

			if (weapons[nextWeaponIndexToTry].CanBeEquipped())
			{
				Debug.Log("Yes!");

				SwapTo(nextWeaponIndexToTry);

				return;
			}
			
			Debug.Log("No :(");

			nextWeaponIndexToTry = WeaponIndexWrapAround(nextWeaponIndexToTry - 1);
		}

		Debug.Log("Nothing else could be equipped. You might need more ammo!");
	}
	
	[ContextMenu("Cycle Down Loop")]
	void CycleDownLoop()
	{
		Debug.Log($"We're going to start looking for the next equippable weapon down, starting from the current weapon [{weapons[currentWeaponIndex].name}");
		
		for (int i = WeaponIndexWrapAround(currentWeaponIndex - 1); i != currentWeaponIndex; i = WeaponIndexWrapAround(i - 1))
		{
			Debug.Log($"Can the [{weapons[i]}] be equipped?");

			if (weapons[i].CanBeEquipped())
			{
				Debug.Log("Yes!");
				
				SwapTo(i);
				
				break;
			}
			else
			{
				Debug.Log("No :(");
			}
		}
		
		Debug.Log("We didn't find an equippable weapon...");
	}

	async void CycleUp()
	{
		int nextWeaponIndexToTry = WeaponIndexWrapAround(currentWeaponIndex + 1);

		Debug.Log($"We're going to start looking for the next equippable weapon up, starting from the current weapon [{currentWeapon.name}");

		while (nextWeaponIndexToTry != currentWeaponIndex) // This will stop when we're back where we started from!
		{
			Debug.Log($"Can the [{weapons[nextWeaponIndexToTry]}] be equipped?");

			if (weapons[nextWeaponIndexToTry].CanBeEquipped())
			{
				Debug.Log("Yes!");

				SwapTo(nextWeaponIndexToTry);

				return;
			}
			
			Debug.Log("No :(");

			nextWeaponIndexToTry = WeaponIndexWrapAround(nextWeaponIndexToTry + 1);
		}

		Debug.Log("Nothing else could be equipped. You might need more ammo!");
	}
	
	// This helps us make sure we never accidentally use a weapon index that doesn't exist!
	// The weapon system would stop working if we let that happen.
	int WeaponIndexWrapAround(int index) => (int) Mathf.Repeat(index, weapons.Count);

	public async void SwapTo(int newWeapon)
	{
		canSwapWeapons = false;

		await UnequipCurrent();

		currentWeaponIndex = newWeapon;

		await EquipCurrent();

		canSwapWeapons = true;
	}

	public async Task UnequipCurrent()
	{
		await currentWeapon.Unequip();

		UShankAnimator.i.ChangeWeaponID(-1); // Makes him to holster whatever he's holding
	}

	public async Task EquipCurrent()
	{
		currentWeapon = weapons[currentWeaponIndex];

		UShankAnimator.i.ChangeWeaponID(currentWeaponIndex); // Makes him unholster new weapon

		await currentWeapon.Equip();

	}
	
}