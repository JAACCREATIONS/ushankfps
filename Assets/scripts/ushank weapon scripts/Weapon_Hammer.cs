﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_Hammer : Weapon
{
    protected override void OnValidate()
    {
        base.OnValidate();
        
        pickedUp = true;
    }
}