﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lookplayer : MonoBehaviour
{
public float xMinium;
public float xMaxium;
    public float speed =0.1f;

    public Vector3 Torsorotation;
    public Vector3 legrotation;
    public Transform Torso;
    public Transform Legs;
    // Start is called before the first frame update
    void Start()
    {
  Torsorotation = Torso.localEulerAngles;
          
    }

    // Update is called once per frame
    void Update()
    {
      if (Torso == null) return;

      
      Torsorotation.x -= Input.GetAxis("Mouse Y");

        Torsorotation.x = Mathf.Clamp(Torsorotation.x,xMinium, xMaxium);
        Torso.localEulerAngles = Torsorotation;

        legrotation.y += Input.GetAxis("Mouse X") *speed;
        Legs.localEulerAngles=legrotation;
        
    }
}
