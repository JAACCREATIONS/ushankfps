﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickupglow : MonoBehaviour
{

    public Material material ;

    public Color from;

    public Color to;

    public float sinrate=1;

    public float sin;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        sin= 0.5f+(Mathf.Sin(Time.time*sinrate)*0.5f);
        var color= Color.Lerp(from,to,sin);

        material.SetColor("tint",color);
    }
}
