﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UShankAnimator : MonoBehaviour
{

    public static UShankAnimator i;
    
    public Animator[] animators = new Animator[2];
    

    private static readonly int WeaponIDProperty = Animator.StringToHash("weaponID");
    private static readonly int AttackProperty = Animator.StringToHash("ATTACK");
    private static readonly int Attack2Property = Animator.StringToHash("ATTACK2");
    private static readonly int ReloadProperty = Animator.StringToHash("RELOAD");

    void Awake()
    {
        i = this;
    }

    public void ChangeWeaponID(int id)
    {
        foreach (var a in animators)
        {
            a.SetInteger(WeaponIDProperty, id);
        }        
    }
    
    public void Attack()
    {
        foreach (var a in animators)
        {
            a.SetTrigger(AttackProperty);
        }
    }

    public void SecondaryAttack()
    {
        foreach (var a in animators)
        {
            a.SetTrigger(Attack2Property);
        }
    }

    public void Reload()
    {
        foreach (var a in animators)
        {
            a.SetTrigger(ReloadProperty);
        }
    }

    
}
