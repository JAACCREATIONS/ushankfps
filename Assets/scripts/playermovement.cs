﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playermovement : MonoBehaviour
{
public bool onTheGround;

  public float speed=0.1f;
  public float deceleration = 1.0f;

  public float groundRayLength = 2.0f;

public LayerMask groundLayer;
public int groundMask;
  public Rigidbody rigidbody;

    // Start is called before the first frame update
    void Start()
    {
    rigidbody = GetComponent<Rigidbody>();    
    groundMask=LayerMask.GetMask("ground");
    }

    // Update is called once per frame
  // Update is called once per frame
    void Update()
    {
      bool inputDetected = false;

     if (Input.GetKey(KeyCode.W)){
         //transform.Translate(Vector3.forward*speed);
rigidbody.AddForce(transform.forward*speed);
inputDetected = true;
     } 
      if (Input.GetKey(KeyCode.S)){
         //transform.Translate(-Vector3.forward*speed);
         rigidbody.AddForce(-transform.forward*speed);
         inputDetected = true;
      }

       if (Input.GetKey(KeyCode.D)){
         //transform.Translate(Vector3.right*speed);
         rigidbody.AddForce(transform.right*speed);
         inputDetected = true;
       }
        if (Input.GetKey(KeyCode.A)){
         //transform.Translate(Vector3.left*speed);
         rigidbody.AddForce(-transform.right*speed);
         inputDetected = true;
    }
var v = rigidbody.velocity;
    if (!inputDetected) {
      
      
      v.x = Mathf.Lerp(v.x, 0, Time.deltaTime * deceleration);
    
      v.z = Mathf.Lerp(v.z, 0, Time.deltaTime * deceleration);

      
        }

        Debug.DrawRay(transform.position, Vector3.down * groundRayLength, Color.blue, 0.1f);

        if (Physics.Raycast(transform.position,Vector3.down,groundRayLength,groundMask)){
          onTheGround=true;
          rigidbody.useGravity=false;
          v.y=0;  
        } else {
          onTheGround=false;
          rigidbody.useGravity=true;
        }

        rigidbody.velocity = v;
    }
}
