﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;



[RequireComponent(typeof(Rigidbody))]
public class PhysicsEntity0 : MonoBehaviour
{
	[SerializeField]
	private bool _onTheGround;
	public bool onTheGround
	{
		get => _onTheGround;
		set
		{
			if (_onTheGround == value) return;
			
			_onTheGround = value;
			
			if (_onTheGround)
			{
				onHitTheGround.Invoke();
			}
			else
			{
				onLeftTheGround.Invoke();
			}
		}
	}
	
	private float drag = 16.0f;
	
	public float height = 1.5f;
	public float heightMargin = 0.1f;
	
	public UnityEvent onLeftTheGround = new UnityEvent();
	public UnityEvent onHitTheGround = new UnityEvent();

	public LayerMask groundLayerMask;
	
	[Header("References")]
	private Rigidbody rigidbody;

//	private Collider collider;
	
	[Header("Air Controls")]
	public bool bounce;
    
	protected virtual void Awake() 
	{
		rigidbody = GetComponent<Rigidbody>();
		
//		collider = GetComponentInChildren<Collider>();
		
		drag = rigidbody.drag;
	}
	
	void FixedUpdate() 
	{
		var velocity = rigidbody.velocity;
		var center = transform.position + new Vector3(0, height, 0);

//		collider.transform.localPosition = new Vector3(0, height, 0);
		
		onTheGround = Physics.Raycast(center, Vector3.down, out var hit, height, groundLayerMask);
		
		if (onTheGround) 
		{
			rigidbody.useGravity = false;
			rigidbody.drag = drag;
			
			if (bounce) 
			{
				velocity.y = -velocity.y * 0.5f;
			}
			else 
			{
				velocity.y = 0.0f;	
			}
			
			transform.position = hit.point - new Vector3(0, heightMargin, 0);
			
			Debug.DrawRay(center, Vector3.down * height, Color.green);
		} 
		else 
		{
			rigidbody.useGravity = true;
			rigidbody.drag = 0.0f;
			
			Debug.DrawRay(center, Vector3.down * height, Color.red);
		}
	}

	void LeftTheGround()
	{
		
	}
	
	void HitTheGround()
	{
		
	}
}