﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weaponpickup : MonoBehaviour
{

    public Transform modeltransform;
    public Vector3 axis = new Vector3(0,1,0);
    public float spinrate = 20.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        modeltransform.Rotate(axis,spinrate * Time.deltaTime);    
    }
}
